import {pactWith} from 'jest-pact';
import {Matchers} from '@pact-foundation/pact';
const pact = require('@pact-foundation/pact-node');

const {like} = Matchers
import {API} from "@/api";

pactWith({
    consumer: "Frontend",
    provider: "Backend",
}, provider => {
    describe("todo process", () => {
        let api
        beforeEach(() => {
            api = new API(provider.mockService.baseUrl, false)
        })
        test('get todo list', async () => {
            const EXPECTED_BODY = [{
                id: like(1),
                description: like("TestTodo")
            }]
            await provider.addInteraction({
                state: 'get todo list successfully',
                uponReceiving: 'a request get todo list',
                withRequest: {
                    method: 'GET',
                    path: '/api/v1/todoList',
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: EXPECTED_BODY
                },
            })
            await api.getTodoList()
        })
        test('add todo', async () => {
            const EXPECTED_BODY = {
                id: like(1),
                description: like("TestTodo")
            }

            await provider.addInteraction({
                state: 'add todo successfully',
                uponReceiving: 'a request adding with description',
                withRequest: {
                    method: 'POST',
                    path: '/api/v1/addTodo',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: {"description": "MyFirstTodo"}
                },
                willRespondWith: {
                    status: 201,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: EXPECTED_BODY
                },
            })
            const res = await api.addTodo("MyFirstTodo")
            expect(res.id).toEqual(1)
        })

    })
    const pactBrokerUrl = process.env.PACT_BROKER_BASE_URL || 'https://ymsagdur.pactflow.io';
    const pactBrokerToken = process.env.PACT_BROKER_TOKEN || 'wrojg1YGLD2UtHGtfPsgNw';

    const gitHash = require('child_process')
        .execSync('git rev-parse --short HEAD')
        .toString().trim();

    const opts = {
        pactFilesOrDirs: ['./pact/pacts/'],
        pactBroker: pactBrokerUrl,
        pactBrokerToken: pactBrokerToken,
        tags: "1.0.0",
        consumerVersion: "1.0.0"
    };

    pact
        .publishPacts(opts)
        .then(() => {

        })
        .catch(e => {
            console.log('Pact contract publishing failed: ', e)
        });
})