import API from '@/api'

jest.mock('@/api')

describe("API", () => {
    it("list all todos", async () => {
        const todos = [
            {id: 1, description: 'Test1'},
            {id: 2, description: 'Test2'},
            {id: 3, description: 'Test3'},
        ]
        API.getTodoList.mockResolvedValue(todos)

        const result = await API.getTodoList()
        expect(result).toEqual(todos)
    })

    it("add todo", async () => {
        const Todo = { id: 1, description: 'Test1' }

        API.addTodo.mockResolvedValue(Todo)

        const result = await API.addTodo(Todo.description)
        expect(result).toEqual(Todo)
    })
})