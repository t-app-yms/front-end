# ToDo List Application
Test
http://ec2-35-175-187-77.compute-1.amazonaws.com

Production
http://ec2-3-80-5-157.compute-1.amazonaws.com

## To Do List

- [x] 1- User interface for ONLY adding ToDo’s
- [x] 2- Back-end service to store a persistent state of ToDo list
- [x] 3- Writing deployment files of your front-end and back-end
- [x] 4- Automating build, test and deployment of your application via Gitlab CI/CD pipeline
- [x] 5- Dockerize both your front-end and back-end application to make them ready for deployment.
- [x] 6- Deploy your application to a AWS ECS cloud provider
- [x] 7- Write deployment configurations(i.e. Dockerfile, AWS configure files etc.) for each project.
-
## Tech Stack For Front-End

<ol>

<li> Vue
    <ol>
        <li>axios (API calls)</li>
        <li>unit tests (vue-test-utils)</li>
        <li>consumer driven contract test (pact-foundation/pact / jest-pact)</li>
    </ol>
</li>

<li>Gitlab (CI-CD)
    .gitlab-ci.yml 
<ol>
    <li>Build</li>
    <li>UnitTest</li>
    <li>test-pact-publish</li>
    <li>dockerize</li>
    <li>deploy2TestEnv</li>
    <li>acceptance-test(trigger)</li>
    <li>Can I Deploy-Pact</li>
    <li>deploy2ProdEnv</li>
    </ol>
</li>

<li>Docker for prod and test env</li>

<li>
Cloud Deployment
<ol>
<li> 2 Amazon Elastic Container Registry (Container Registry) (Front-end / Back-end)  </li>
<li> 2 Amazon Elastic Container Service (2 EC2 for deploy) (Front-end / Back-end)  </li>
</ol>
</li>
</ol>


## Pipeline automation

Build

The first step is to build the project. If there is an error in the code, it will not be able to build, and we learn this early.
We can get build manually in the code below.

`$ - yarn install`
`$ - yarn build`

Test

I thought I should separate testing from pact testing. because I will use different image for unit tests, different image pact test
I made the following change in package.json to separate it. This way, when I type `yarn test:unit`, only unit tests will run.

```
"test:unit": "vue-cli-service test:unit --testPathIgnorePatterns=pacts/",
"test:pact": "vue-cli-service test:unit pacts/",
```

When the unit tests run successfully, it starts the next step.

Test-pact-publish 

I publish my pact test with pactflow

Dockerize

After starting the docker process using nginx, after making my ECR connection operations, I push my docker to the ecr container

Example
![](images/ecr-frontend1.png)
![](images/ecr-frontend2.png)

Deploy2TestEnv

The docker that I registered with ECR runs the ecr container that I specified in my task via the ECS service on the EC2 virtual machine. But I am sending the test variable to dockerima on gitlab so that vuejs will know that there is a test environment while running and apply api operations accordingly.

![](images/TestEnvCluster.png)

Acceptance-Test

I triggered the pipeline I created in the Acceptance repository when I got the required tokens here.

Can I Deploy-Pact

Thanks to pactflow, I'm checking the conract I verified. If a successful result is returned, it means that I can deploy now.

![](images/pact.png)

Dockerize-for-prod

Here, if the acceptance passes successfully, I need to deploy to the prod. I need to dockerize again before this because I sent the test variable of the previous docker process, now I am sending the production variable and pushing it to ecr again

Deploy2ProdEnv

If all processes pass, I now broadcast to my real environment because I need to move it to the live environment.

![](images/prodenv.png)


###Pipeline Layout

![](images/pipeline-front1.png)

![](images/pipeline-front2.png)


Successfully Completed.

## Development enviroment

### Project Setup
To install the packages first

`yarn install`

After to start the project in the development environment

`yarn serve --mode development`

Start the project in the test environment

`yarn serve --mode test`

Start the project in the production environment

`yarn serve --mode production`

To run unit tests

`yarn test:unit`

To run pact tests

`yarn test:pact`

![](images/app.png)

