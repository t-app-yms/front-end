import TodoOperation from "@/components/TodoOperation";
import {shallowMount} from "@vue/test-utils";
import API from "@/api";

jest.mock('@/api')

describe("TodoOperation.vue", () => {
    describe("exists check", () => {
        let wrapper
        beforeEach(() => {
            wrapper = shallowMount(TodoOperation)
        })
        it("should component exists", () => {
            expect(wrapper.exists()).toBeTruthy()
            expect(wrapper).toMatchSnapshot()

        })
        it("should render text box", () => {
            const input = wrapper.find('#todo-text')
            expect(input.exists()).toBeTruthy()
        })
        it("should render add button", () => {
            const button = wrapper.find('#add-todo')
            expect(button.exists()).toBeTruthy()
        })
        it("empty description and click add button", async () => {
            wrapper = shallowMount(TodoOperation, {
                data() {
                    return {
                        todo: '',
                        task: ""
                    }
                },
            })

            const DescriptionTodo = ""
            const todoText = wrapper.find('#todo-text')
            await todoText.setValue(DescriptionTodo)

            const addButton = wrapper.find('#add-todo')
            addButton.trigger('click')

            expect(wrapper.vm.$data.todo).toEqual(DescriptionTodo)
            expect(wrapper.vm.$data.task).toEqual("Empty")
            expect(todoText.element.value).toEqual(DescriptionTodo)

        })
        it("non-empty description and click add button correctly", async () => {
            wrapper = shallowMount(TodoOperation, {
                data() {
                    return {
                        todo: '',
                        task: ""
                    }
                },
            })
            const mockResponse = [
                {
                    "id": 1,
                    "description": "Test1",
                },
            ]
            API.addTodo.mockResolvedValue(mockResponse)

            const DescriptionTodo = "Test1"

            const todoText = wrapper.find('#todo-text')
            await todoText.setValue(DescriptionTodo)

            const addButton = wrapper.find('#add-todo')
            await addButton.trigger('click')

            expect(API.addTodo).toHaveBeenCalledTimes(1)
            expect(wrapper.vm.$data.todo).toEqual(DescriptionTodo)
            expect(todoText.element.value).toEqual(DescriptionTodo)

            await wrapper.vm.$nextTick()

            expect(wrapper.emitted()).toHaveProperty('todo')
            expect(wrapper.vm.$data.task).toEqual([{"id": 1,"description": "Test1"}])
            expect(todoText.element.value).toEqual("")


        })
    })
})