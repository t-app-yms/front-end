import List from "@/components/List";
import {shallowMount} from "@vue/test-utils";

describe("List.vue", () => {
    describe("exists check", () => {
        const factory = (options) => {
            return shallowMount(List, {
                propsData: {
                    ...options
                }
            })
        }
        let wrapper = factory({todo: {}})

        it("should component exists", () => {
            expect(wrapper.exists()).toBeTruthy()
        })
        it("should render component li", () => {
            const todo = wrapper.find('li')
            expect(todo.exists()).toBeTruthy()
        })

        it('as for the prop data, check if the printing is correct', () => {
            const todo = {
                "id": 1,
                "description": "MyTodo",
            }
            wrapper = factory({todo: todo})
            expect(wrapper.exists()).toBeTruthy()
            expect(wrapper.find('li').text()).toEqual( todo.id + ". " + todo.description)
            expect(wrapper).toMatchSnapshot()

        })
    })
})