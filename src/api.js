import axios from 'axios'
import adapter from "axios/lib/adapters/http";

axios.defaults.adapter = adapter;


export class API {
    useProxy = false
    constructor(url, useProxy) {
        this.useProxy = useProxy
        if (url === undefined || url === "") {
            url = process.env.VUE_APP_BASE_API_URL;
        }
        this.url = url
    }

    withPath(path) {
        // if(this.useProxy){
        //     return path
        // }
        if (!path.startsWith("/")) {
            path = "/" + path
        }
        return `${this.url}${path}`
    }

    async getTodoList() {
        return axios.get(this.withPath('api/v1/todoList')).then(r => r.data)
    }

    async addTodo(description) {
        return axios.post(this.withPath('api/v1/addTodo'), {description: description}).then(r => r.data)
    }

}

export default new API(process.env.VUE_APP_BASE_API_URL,true);