import {shallowMount} from "@vue/test-utils";
import Home from "@/views/Home";
import API from "@/api";
import flushPromises from "flush-promises";
import List from "@/components/List";
import TodoOperation from "@/components/TodoOperation";

jest.mock('@/api')

describe("Home.vue", () => {
    it("should component exists", () => {
        const wrapper = shallowMount(Home)
        expect(wrapper.exists()).toBeTruthy()
    })
    it("should h1 component exists", () => {
        const wrapper = shallowMount(Home)
        const todo = wrapper.find('h1')
        const text = wrapper.find("h1").text()
        expect(text).toEqual("Modanisa Todo List App")
        expect(todo.exists()).toBeTruthy()
    })
    it("should component exists todo", () => {
        API.getTodoList.mockResolvedValue([])
        const wrapper = shallowMount(Home)
        expect(wrapper.exists()).toBeTruthy()
    })
    it("should render todo list item components correctly", async () => {
        const mockResponse = [
            {
                "id": 1,
                "description": "Test1",
            },
            {
                "id": 2,
                "description": "Test2",
            },
            {
                "id": 3,
                "description": "Test3",
            }
        ]
        API.getTodoList.mockResolvedValue(mockResponse)
        const wrapper = shallowMount(Home)

        await flushPromises()
        const todoComponents = wrapper.findAllComponents(List)
        expect(todoComponents).toHaveLength(mockResponse.length)
    })
    it("addTodo func correctly", async () => {
        const spy = jest.spyOn(Home.methods, 'addTodo')

        const wrapper = shallowMount(Home, {
            data() {
                return {
                    todos: [{"id": 1, "description": "TestTodo1"}],
                }
            },
        })
        wrapper.findComponent(TodoOperation).vm.$emit('todo', {"id": 2, "description": "TestTodo2"})
        expect(spy).toHaveBeenCalled()
        const expected = [{"id": 1, "description": "TestTodo1"}, {"id": 2, "description": "TestTodo2"}]
        expect(wrapper.vm.$data.todos).toEqual(expected)
    })

    it("addTodo same todo", async () => {
        const spy = jest.spyOn(Home.methods, 'addTodo')

        const wrapper = shallowMount(Home, {
            data() {
                return {
                    todos: [{"id": 1, "description": "TestTodo1"}],
                }
            },
        })

        wrapper.findComponent(TodoOperation).vm.$emit('todo', {"id": 1, "description": "TestTodo1"})
        expect(spy).toHaveBeenCalled()
        const expected = [{"id": 1, "description": "TestTodo1"}]
        expect(wrapper.vm.$data.todos).toEqual(expected)
        expect(wrapper).toMatchSnapshot()

    })
})
