FROM node:lts-alpine as build-stage
ARG ENV
WORKDIR /app
COPY package*.json ./
RUN yarn install
COPY . .
RUN yarn run build --mode $ENV

FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]